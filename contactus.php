<?php include 'includes/header.php' ?>
<?php include 'includes/header-inside.php' ?>

<div class="bg-blue py-5">
    <div class="container">
        <div class="custom-breadcrumbs">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php"><i class="icon-home"></i> Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">CONTACT US</li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<!-- Google Map -->
<div class="google-map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d7290573.081382147!2d35.373383855720085!3d26.844718475551225!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sar!2seg!4v1570377395578!5m2!1sar!2seg" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
</div>

<div class="py-5 bg-white page-content overflow-hidden">
    <div class="container">
        <h3 class="section-title mb-4 mb-md-5">CONTACT US</h3>
        <div class="row">
            <div class="col-12 col-lg-7 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1s">
                <form action="contact.php" method="post" class="contact-form">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Mobile">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" placeholder="Message" rows="6"></textarea>
                    </div>
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-yellow text-white">Send <i class="icon-double-arrow"></i></button>
                    </div>

                </form>
            </div>
            <div class="col-12 col-lg-5 mt-4 mt-lg-0 wow fadeInRight text-blue" data-wow-duration="1s" data-wow-delay="1s">
                <div class="contact-data-item mb-4">
                    <i class="icon-periscope text-yellow"></i>
                    <p>
                        Lorem Ipsum is simply dummy text of the
                        printing and typesetting industry.
                    </p>
                </div>
                <div class="contact-data-item mb-4">
                    <i class="icon-smartphone text-yellow"></i>
                    <p>0123456789</p>
                </div>
                <div class="contact-data-item mb-4">
                    <i class="icon-envelope text-yellow"></i>
                    <p><a href="mailto:info@abilita.com" class="text-blue text-yellow-hover">info@abilita.com</a></p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'includes/footer.php' ?>
