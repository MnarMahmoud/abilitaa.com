$(document).ready(function(){

    var hero = $ ('.owl-carousel');
    $("#main-banner").owlCarousel({
        items:1,
        loop:true,
        autoplay:true,
        nav:false,
        autoplayTimeout:5000,
        animateOut: 'fadeOut'
    });


    hero.on("changed.owl.carousel",function (event) {
        var itemxx = event.item.index-2;
        $('h1').removeClass('animated fadeInDown');
        $(".owl-item").not('.cloned').eq(itemxx).find('h1').addClass('animated fadeInDown');
    });


    $(".owl-5-items").owlCarousel({
        items:5,
        loop:true,
        autoplay:true,
        margin:25,
        responsive:{
            0:{
                items:2,
                nav:false,
                dots:true,
            },
            600:{
                items:3,
                nav:false,
                dots:true,
            },
            1000:{
                items:5,
                nav:true,
                dots:false,
                navText: ["<i class='icon-double-arrow rotate-left'></i>","<i class='icon-double-arrow'></i>"]
            }
        }
    });

    $(window).scroll(function(){
        if($(this).scrollTop() > 50 && $(this).width() > 768) {
            $('.main-navigation').addClass('fixed-nav');
        }
        else {
            $('.main-navigation').removeClass('fixed-nav');
        }
    });

    new WOW().init({
        mobile: false,
    });

});