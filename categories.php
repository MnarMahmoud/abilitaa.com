<?php include 'includes/header.php' ?>
<?php include 'includes/header-inside.php' ?>

<div class="bg-blue py-5">
    <div class="container">
        <div class="custom-breadcrumbs">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php"><i class="icon-home"></i> Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">CATEGORIES</li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="py-5 bg-white page-content overflow-hidden">
    <div class="container">
        <h3 class="section-title mb-4 mb-md-5">CATEGORIES</h3>
        <div class="row">
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-4 mb-md-5 wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
                <div class="category-item"><a href="service-details.php">
                    <div class="overflow-hidden">
                        <div class="image-container" style="background-image: url('assets/images/about-us.jpg');"></div>
                    </div>
                    <h3 class="py-3"><a href="service-details.php" class="category-title">Category Title</a></h3></a>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-4 mb-md-5 wow fadeIn" data-wow-duration="1s" data-wow-delay=".5s">
                <div class="category-item"><a href="service-details.php">
                    <div class="overflow-hidden">
                        <div class="image-container" style="background-image: url('assets/images/austin-distel-wD1LRb9OeEo-unsplash.jpg');"></div>
                    </div>
                    <h3 class="py-3"><a href="service-details.php" class="category-title">Category Title</a></h3></a>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-4 mb-md-5 wow fadeIn" data-wow-duration="1s" data-wow-delay=".7s">
                <div class="category-item"><a href="service-details.php">
                    <div class="overflow-hidden">
                        <div class="image-container" style="background-image: url('assets/images/what-we-do.jpg');"></div>
                    </div>
                    <h3 class="py-3"><a href="service-details.php" class="category-title">Category Title</a></h3></a>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-4 mb-md-5 wow fadeIn" data-wow-duration="1s" data-wow-delay=".9s">
                <div class="category-item"><a href="service-details.php">
                    <div class="overflow-hidden">
                        <div class="image-container" style="background-image: url('assets/images/banner1.jpg');"></div>
                    </div>
                    <h3 class="py-3"><a href="service-details.php" class="category-title">Category Title</a></h3></a>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-4 mb-md-5 wow fadeIn" data-wow-duration="1s" data-wow-delay="1.1s">
                <div class="category-item"><a href="service-details.php">
                    <div class="overflow-hidden">
                        <div class="image-container" style="background-image: url('assets/images/banner2.jpg');"></div>
                    </div>
                    <h3 class="py-3"><a href="service-details.php" class="category-title">Category Title</a></h3></a>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-4 mb-md-5 wow fadeIn" data-wow-duration="1s" data-wow-delay="1.3s">
                <div class="category-item"><a href="service-details.php">
                    <div class="overflow-hidden">
                        <div class="image-container" style="background-image: url('assets/images/banner3.jpg');"></div>
                    </div>
                    <h3 class="py-3"><a href="service-details.php" class="category-title">Category Title</a></h3></a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'includes/footer.php' ?>
