<footer>
    <div class="bg-blue py-4 py-lg-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-5 col-lg-4">
                    <img src="assets/images/logo-white.png" alt="Abilita" class="img-logo">
                    <p class="mt-3">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry's standard dummy
                    </p>
                </div>
                <div class="col-12 col-md-2 col-lg-2 offset-lg-2 mt-3 mb-2 my-md-0">
                    <div class="footer-navigation">
                        <ul class="nav flex-column">
                            <li><a href="#"><i class="icon-double-arrow"></i> Home</a></li>
                            <li><a href="#"><i class="icon-double-arrow"></i> About Us</a></li>
                            <li><a href="#"><i class="icon-double-arrow"></i> Services</a></li>
                            <li><a href="#"><i class="icon-double-arrow"></i> Contact Us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-5 col-lg-4">
                    <p class="mb-2"><i class="icon-periscope"></i>
                        Lorem Ipsum is simply dummy text of the
                        printing and typesetting industry.</p>
                    <p class="mb-2 dir-left"><i class="icon-smartphone"></i> 0123456789</p>
                    <p class="mb-2 dir-left"><i class="icon-envelope"></i> info@abilita.com</p>
                </div>
            </div>
        </div>
    </div>
    <div class="copy-rights">
        <div class="container py-3">
            <div class="row">
                <div class="col-12 col-md-6 text-center text-md-left">
                    <p>copyright © 2019 <a href="index.php">ALIBUTA</a> </p>
                </div>
                <div class="col-12 col-md-6 mt-3 mt-md-0">
                    <div class="social-media-icons">
                        <ul class="nav">
                            <li><a href="#" target="_blank"><i class="icon-facebook"></i></a></li>
                            <li><a href="#" target="_blank"><i class="icon-twitter"></i></a></li>
                            <li><a href="#" target="_blank"><i class="icon-whatsapp"></i></a></li>
                            <li><a href="#" target="_blank"><i class="icon-youtube"></i></a></li>
                            <li><a href="#" target="_blank"><i class="icon-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php include 'scripts.php' ?>

</body>
</html>