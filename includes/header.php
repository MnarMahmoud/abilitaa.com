<?php include 'head.php' ?>

<?php $link = $_SERVER['PHP_SELF'];
$link_array = explode('/',$link);
$page = end($link_array);
?>

<header>
    <div class="top-header bg-blue py-2">
        <div class="container">
            <div class="row">
                <div class="col-6 col-md-4 col-lg-2">
                    <p><a href="mailto:info@abilita.com"><i class="icon-envelope"></i> info@abilita.com</a></p>
                </div>
                <div class="col-6 col-md-4 col-lg-2 text-right text-md-left">
                    <p><i class="icon-call"></i> 012345678</p>
                </div>
                <div class="col-12 col-md-4 col-lg-8 mt-2 mt-md-0">
                    <div class="social-media-icons">
                        <ul class="nav">
                            <li><a href="#" target="_blank"><i class="icon-facebook"></i></a></li>
                            <li><a href="#" target="_blank"><i class="icon-twitter"></i></a></li>
                            <li><a href="#" target="_blank"><i class="icon-whatsapp"></i></a></li>
                            <li><a href="#" target="_blank"><i class="icon-youtube"></i></a></li>
                            <li><a href="#" target="_blank"><i class="icon-instagram"></i></a></li>
                            <li><a href="#" class="ar-lang">عربي</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>


