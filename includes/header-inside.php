<div class="main-navigation bg-white position-static">
    <div class="container py-2 py-md-3">
        <div class="row">
            <div class="col-lg-2 d-none d-lg-block">
                <a href="index.php"><img src="assets/images/logo-abilita.png" alt="abilitaa" class="logo-image"></a>
            </div>
            <div class="col-12 col-lg-10">
                <nav class="navbar navbar-expand-lg p-0 h-100">
                    <a class="navbar-brand d-lg-none" href="index.php"><img src="assets/images/logo-abilita.png" alt="abilitaa" class="logo-image"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navigation" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="icon-list"></i>
                    </button>

                    <div class="collapse navbar-collapse" id="main-navigation">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item <?php if ($page=='index.php'):?>active<?php endif;?>">
                                <a class="nav-link" href="index.php"> <i class="icon-home"></i> Home</a>
                            </li>
                            <li class="nav-item <?php if ($page=='aboutus.php'):?>active<?php endif;?>">
                                <a class="nav-link" href="aboutus.php">About Us</a>
                            </li>
                            <li class="nav-item <?php if ($page=='services.php'):?>active<?php endif;?>">
                                <a class="nav-link" href="services.php">Services</a>
                            </li>
                            <li class="nav-item <?php if ($page=='categories.php'):?>active<?php endif;?>">
                                <a class="nav-link" href="categories.php">Categories</a>
                            </li>
                            <li class="nav-item <?php if ($page=='contactus.php'):?>active<?php endif;?>">
                                <a class="nav-link" href="contactus.php">Contact Us</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>