<?php include 'includes/header.php' ?>
<?php include 'includes/header-inside.php' ?>

<div class="bg-blue py-5">
    <div class="container">
        <div class="custom-breadcrumbs">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php"><i class="icon-home"></i> Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">LOGO DESIGN</li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="py-5 bg-white page-content overflow-hidden">
    <div class="container">
        <h3 class="section-title mb-4">LOGO DESIGN</h3>
        <div class="row">
            <div class="col-12 col-lg-7 wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1s">
                <p><b>All we do, we do with a vision.</b> <br><br>
                    Specifically, an eagle’s eye piercing vision. <br><br>
                    So that gives us the capability to see things from a much wider perspective. You see, we believe that
                    every problem has more than one solution. And it’s our job to evaluate all those solutions and decide
                    which one will be the most efficient for you; giving you the biggest impact and the most effective
                    outcome. <br><br>
                    Specifically, an eagle’s eye piercing vision. <br><br>
                    So that gives us the capability to see things from a much wider perspective. You see, we believe that
                    every problem has more than one solution. And it’s our job to evaluate all those solutions and decide
                    which one will be the most efficient for you; giving you the biggest impact and the most effective
                    outcome.
                </p>
            </div>
            <div class="col-12 col-lg-5 mt-4 mt-lg-0 wow fadeInRight" data-wow-duration="1s" data-wow-delay="1s">
                <img src="assets/images/austin-distel-wD1LRb9OeEo-unsplash.jpg" alt="ABOUT US" class="img-fluid">
            </div>
        </div>
    </div>
</div>

<?php include 'includes/footer.php' ?>
