<?php include 'includes/header.php' ?>
<?php include 'includes/header-inside.php' ?>

<div class="bg-blue py-5">
    <div class="container">
        <div class="custom-breadcrumbs">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="index.php"><i class="icon-home"></i> Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">OUR SERVICES</li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="my-4 my-lg-5">
    <div class="container">
        <h4 class="mb-4 mb-md-5 text-blue section-title">OUR SERVICES</h4>
        <div class="row">
            <div class="col-12 col-sm-6 col-lg-4 mb-4 wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
                <div class="bg-white p-3 service-item">
                    <div class="row">
                        <div class="col-12 col-md-8 text-center text-md-left">
                            <h5 class="mb-2"><a href="service-details.php">LOGO DESIGN</a></h5>
                            <p>So that gives us the capability to see
                                things from a much wider perspective.
                                <a href="service-details.php" class="read-more">Read More</a>
                            </p>
                        </div>
                        <div class="col-6 col-md-4 pl-0 offset-3 offset-md-0 mt-3 mt-md-0">
                            <img src="assets/images/seo-gif-blue.gif" alt="LOGO DESIGN" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-4 mb-4 wow fadeIn" data-wow-duration="1s" data-wow-delay=".6s">
                <div class="bg-white p-3 service-item">
                    <div class="row">
                        <div class="col-12 col-md-8 text-center text-md-left">
                            <h5 class="mb-2"><a href="service-details.php">WEBSITE DESIGN</a></h5>
                            <p>So that gives us the capability to see
                                things from a much wider perspective.
                                <a href="service-details.php" class="read-more">Read More</a>
                            </p>
                        </div>
                        <div class="col-6 col-md-4 pl-0 offset-3 offset-md-0 mt-3 mt-md-0">
                            <img src="assets/images/branding-gif-blue.gif" alt="LOGO DESIGN" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-4 mb-4 wow fadeIn" data-wow-duration="1s" data-wow-delay=".9s">
                <div class="bg-white p-3 service-item">
                    <div class="row">
                        <div class="col-12 col-md-8 text-center text-md-left">
                            <h5 class="mb-2"><a href="service-details.php">DIGITAL MARKETING</a></h5>
                            <p>So that gives us the capability to see
                                things from a much wider perspective.
                                <a href="service-details.php" class="read-more">Read More </a>
                            </p>
                        </div>
                        <div class="col-6 col-md-4 pl-0 offset-3 offset-md-0 mt-3 mt-md-0">
                            <img src="assets/images/digital-gif-blue.gif" alt="LOGO DESIGN" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-4 mb-4 wow fadeIn" data-wow-duration="1s" data-wow-delay="1.2s">
                <div class="bg-white p-3 service-item">
                    <div class="row">
                        <div class="col-12 col-md-8 text-center text-md-left">
                            <h5 class="mb-2"><a href="service-details.php">SOCIAL MEDIA</a></h5>
                            <p>So that gives us the capability to see
                                things from a much wider perspective.
                                <a href="service-details.php" class="read-more">Read More</a>
                            </p>
                        </div>
                        <div class="col-6 col-md-4 pl-0 offset-3 offset-md-0 mt-3 mt-md-0">
                            <img src="assets/images/soical-gif-blue.gif" alt="LOGO DESIGN" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-4 mb-4 wow fadeIn" data-wow-duration="1s" data-wow-delay="1.5s">
                <div class="bg-white p-3 service-item">
                    <div class="row">
                        <div class="col-12 col-md-8 text-center text-md-left">
                            <h5 class="mb-2"><a href="service-details.php">CREATIVE ADVERTISING</a></h5>
                            <p>So that gives us the capability to see
                                things from a much wider perspective.
                                <a href="service-details.php" class="read-more">Read More</a>
                            </p>
                        </div>
                        <div class="col-6 col-md-4 pl-0 offset-3 offset-md-0 mt-3 mt-md-0">
                            <img src="assets/images/web-blue-animated.gif" alt="LOGO DESIGN" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-4 mb-4 wow fadeIn" data-wow-duration="1s" data-wow-delay="1.8s">
                <div class="bg-white p-3 service-item">
                    <div class="row">
                        <div class="col-12 col-md-8 text-center text-md-left">
                            <h5 class="mb-2"><a href="service-details.php">MOBILE APPLICATION</a></h5>
                            <p>So that gives us the capability to see
                                things from a much wider perspective.
                                <a href="service-details.php" class="read-more">Read More</a>
                            </p>
                        </div>
                        <div class="col-6 col-md-4 pl-0 offset-3 offset-md-0 mt-3 mt-md-0">
                            <img src="assets/images/content-gif-blue.gif" alt="LOGO DESIGN" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'includes/footer.php' ?>
