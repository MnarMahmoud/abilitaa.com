 <?php include 'includes/header.php' ?>

<!-- Main Banner-->
<div class="main-banner">
    <div class="main-navigation">
        <div class="container py-2 py-md-3">
            <div class="row">
                <div class="col-lg-2 d-none d-lg-block">
                    <a href="index.php"><img src="assets/images/logo-abilita.png" alt="abilitaa" class="logo-image"></a>
                </div>
                <div class="col-12 col-lg-10">
                    <nav class="navbar navbar-expand-lg p-0 h-100">
                        <a class="navbar-brand d-lg-none" href="index.php"><img src="assets/images/logo-abilita.png" alt="abilitaa" class="logo-image"></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navigation" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <i class="icon-list"></i>
                        </button>

                        <div class="collapse navbar-collapse" id="main-navigation">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item <?php if ($page=='index.php'):?>active<?php endif;?>">
                                    <a class="nav-link" href="index.php"> <i class="icon-home"></i> Home</a>
                                </li>
                                <li class="nav-item <?php if ($page=='aboutus.php'):?>active<?php endif;?>">
                                    <a class="nav-link" href="aboutus.php">About Us</a>
                                </li>
                                <li class="nav-item <?php if ($page=='services.php'):?>active<?php endif;?>">
                                    <a class="nav-link" href="services.php">Services</a>
                                </li>
                                <li class="nav-item <?php if ($page=='categories.php'):?>active<?php endif;?>">
                                    <a class="nav-link" href="categories.php">Categories</a>
                                </li>
                                <li class="nav-item <?php if ($page=='contactus.php'):?>active<?php endif;?>">
                                    <a class="nav-link" href="contactus.php">Contact Us</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="owl-carousel owl-theme" id="main-banner">
        <div class="image-container" style="background-image: url('assets/images/banner1.jpg');">
           <div class="container">
               <div class="caption">
                   <h1 class="animated fadeInDown mb-3">Free your Vision</h1>
               </div>
           </div>
        </div>
        <div class="image-container" style="background-image: url('assets/images/banner2.jpg');">
            <div class="container">
                <div class="caption">
                    <h1 class="animated fadeInDown mb-3">Branding-Advertising</h1>
                    <h2 class="d-none d-md-block">Invade market with strong brand -By creating strong marketing plan</h2>
                </div>
            </div>
        </div>
        <div class="image-container" style="background-image: url('assets/images/banner3.jpg');">
            <div class="container">
                <div class="caption">
                    <h1 class="animated fadeInDown mb-3">Develop outstanding websites</h1>
                    <h2 class="d-none d-md-block">and mobile apps by creative solutions to represent your brand in the digital world</h2>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- About Us -->
<div class="bg-white py-4 py-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 pt-4 mb-4 mb-md-0">
                <div class="square-border square-border-image-left">
                    <img src="assets/images/austin-distel-wD1LRb9OeEo-unsplash.jpg" alt="ABOUT US" class="img-fluid">
                </div>
            </div>
            <div class="col-12 col-md-6">
                <h4 class="mb-4 text-blue section-title">ABOUT US</h4>
                <p><b>All we do, we do with a vision.</b> <br><br>
                    Specifically, an eagle’s eye piercing vision. <br><br>
                    So that gives us the capability to see things from a much wider perspective. You see, we believe that
                    every problem has more than one solution. And it’s our job to evaluate all those solutions and decide
                    which one will be the most efficient for you; giving you the biggest impact and the most effective
                    outcome.
                </p>
                <div class="text-center text-md-right mt-3 mt-md-4">
                    <a href="aboutus.php" class="btn btn-yellow text-white">
                        Read More <i class="icon-double-arrow"></i></a>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- Services -->
<div class="my-4 my-lg-5">
    <div class="container">
        <h4 class="mb-4 mb-md-5 text-blue section-title">OUR SERVICES</h4>
        <div class="row">
            <div class="col-12 col-sm-6 col-lg-4 mb-4 wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
                <div class="bg-white p-3 service-item">
                    <div class="row">
                        <div class="col-12 col-md-8 text-center text-md-left">
                            <h5 class="mb-2"><a href="service-details.php">LOGO DESIGN</a></h5>
                            <p>So that gives us the capability to see
                                things from a much wider perspective.
                                <a href="service-details.php" class="read-more">Read More</a>
                            </p>
                        </div>
                        <div class="col-6 col-md-4 pl-0 offset-3 offset-md-0 mt-3 mt-md-0">
                            <img src="assets/images/seo-gif-blue.gif" alt="LOGO DESIGN" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-4 mb-4 wow fadeIn" data-wow-duration="1s" data-wow-delay=".6s">
                <div class="bg-white p-3 service-item">
                    <div class="row">
                        <div class="col-12 col-md-8 text-center text-md-left">
                            <h5 class="mb-2"><a href="service-details.php">WEBSITE DESIGN</a></h5>
                            <p>So that gives us the capability to see
                                things from a much wider perspective.
                                <a href="service-details.php" class="read-more">Read More</a>
                            </p>
                        </div>
                        <div class="col-6 col-md-4 pl-0 offset-3 offset-md-0 mt-3 mt-md-0">
                            <img src="assets/images/branding-gif-blue.gif" alt="LOGO DESIGN" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-4 mb-4 wow fadeIn" data-wow-duration="1s" data-wow-delay=".9s">
                <div class="bg-white p-3 service-item">
                    <div class="row">
                        <div class="col-12 col-md-8 text-center text-md-left">
                            <h5 class="mb-2"><a href="service-details.php">DIGITAL MARKETING</a></h5>
                            <p>So that gives us the capability to see
                                things from a much wider perspective.
                                <a href="service-details.php" class="read-more">Read More </a>
                            </p>
                        </div>
                        <div class="col-6 col-md-4 pl-0 offset-3 offset-md-0 mt-3 mt-md-0">
                            <img src="assets/images/digital-gif-blue.gif" alt="LOGO DESIGN" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-4 mb-4 wow fadeIn" data-wow-duration="1s" data-wow-delay="1.2s">
                <div class="bg-white p-3 service-item">
                    <div class="row">
                        <div class="col-12 col-md-8 text-center text-md-left">
                            <h5 class="mb-2"><a href="service-details.php">SOCIAL MEDIA</a></h5>
                            <p>So that gives us the capability to see
                                things from a much wider perspective.
                                <a href="service-details.php" class="read-more">Read More</a>
                            </p>
                        </div>
                        <div class="col-6 col-md-4 pl-0 offset-3 offset-md-0 mt-3 mt-md-0">
                            <img src="assets/images/soical-gif-blue.gif" alt="LOGO DESIGN" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-4 mb-4 wow fadeIn" data-wow-duration="1s" data-wow-delay="1.5s">
                <div class="bg-white p-3 service-item">
                    <div class="row">
                        <div class="col-12 col-md-8 text-center text-md-left">
                            <h5 class="mb-2"><a href="service-details.php">CREATIVE ADVERTISING</a></h5>
                            <p>So that gives us the capability to see
                                things from a much wider perspective.
                                <a href="service-details.php" class="read-more">Read More</a>
                            </p>
                        </div>
                        <div class="col-6 col-md-4 pl-0 offset-3 offset-md-0 mt-3 mt-md-0">
                            <img src="assets/images/web-blue-animated.gif" alt="LOGO DESIGN" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-lg-4 mb-4 wow fadeIn" data-wow-duration="1s" data-wow-delay="1.8s">
                <div class="bg-white p-3 service-item">
                    <div class="row">
                        <div class="col-12 col-md-8 text-center text-md-left">
                            <h5 class="mb-2"><a href="service-details.php">MOBILE APPLICATION</a></h5>
                            <p>So that gives us the capability to see
                                things from a much wider perspective.
                                <a href="service-details.php" class="read-more">Read More</a>
                            </p>
                        </div>
                        <div class="col-6 col-md-4 pl-0 offset-3 offset-md-0 mt-3 mt-md-0">
                            <img src="assets/images/content-gif-blue.gif" alt="LOGO DESIGN" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- What We Do -->
<div class="bg-white mt-4 mt-lg-5 py-4 py-lg-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <h4 class="mb-4 text-blue section-title">WHAT WE DO</h4>
                <p><b>All we do, we do with a vision.</b> <br><br>
                    Specifically, an eagle’s eye piercing vision. <br><br>
                    So that gives us the capability to see things from a much wider perspective. You see, we believe that
                    every problem has more than one solution. And it’s our job to evaluate all those solutions and decide
                    which one will be the most efficient for you; giving you the biggest impact and the most effective
                    outcome.
                </p>
                <div class="text-center text-md-right mt-3">
                    <a href="aboutus.php" class="btn btn-yellow text-white">Read More <i class="icon-double-arrow"></i></a>
                </div>
            </div>
            <div class="col-12 col-md-6 mt-4 mt-md-0">
                <div class="square-border">
                    <img src="assets/images/what-we-do.jpg" alt="what we do" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Success Partners -->
<div class="py-4 py-lg-5">
    <div class="container">
        <h4 class="mb-4 mb-lg-5 text-blue section-title">Success Partners</h4>
        <div class="owl-carousel owl-theme owl-5-items success-partner">
            <?php for ($x = 1; $x <= 5; $x++) { ?>
                <img src="assets/images/logo-<?php echo $x;?>.png" alt="Partner Name" class="img-fluid">
            <?php } ?>
            <?php for ($x = 1; $x <= 5; $x++) { ?>
                <img src="assets/images/logo-<?php echo $x;?>.png" alt="Partner Name" class="img-fluid">
            <?php } ?>
        </div>
    </div>
</div>

<?php include 'includes/footer.php' ?>
